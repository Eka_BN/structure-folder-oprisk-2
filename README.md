# Folder Strucutre 2
```sh
└───module
    └───rcsa
        ├───maintance
        │   ├───ketentuan
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       └───edit
        │   │   ├───models
        │   │   ├───services
        │   │   └───store
        │   │       ├───actions
        │   │       ├───effects
        │   │       ├───reducer
        │   │       └───selector
        │   ├───key-control
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       └───edit
        │   │   ├───models
        │   │   ├───services
        │   │   └───store
        │   │       ├───actions
        │   │       ├───effects
        │   │       ├───reducer
        │   │       └───selector
        │   ├───key-process
        │   │   ├───jaringan
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       └───edit
        │   │   │   ├───models
        │   │   │   ├───services
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       ├───effects
        │   │   │       ├───reducer
        │   │   │       └───selector
        │   │   └───kantor-pusat
        │   │       ├───components
        │   │       │   ├───main
        │   │       │   └───modals
        │   │       │       ├───add
        │   │       │       └───edit
        │   │       ├───models
        │   │       ├───services
        │   │       └───store
        │   │           ├───actions
        │   │           ├───effects
        │   │           ├───reducer
        │   │           └───selector
        │   ├───key-risk
        │   │   ├───jaringan
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       └───edit
        │   │   │   ├───models
        │   │   │   ├───services
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       ├───effects
        │   │   │       ├───reducer
        │   │   │       └───selector
        │   │   └───kantor-pusat
        │   │       ├───components
        │   │       │   ├───main
        │   │       │   └───modals
        │   │       │       ├───add
        │   │       │       └───edit
        │   │       ├───models
        │   │       ├───services
        │   │       └───store
        │   │           ├───actions
        │   │           ├───effects
        │   │           ├───reducer
        │   │           └───selector
        │   ├───let
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       └───edit
        │   │   ├───models
        │   │   ├───services
        │   │   └───store
        │   │       ├───actions
        │   │       ├───effects
        │   │       ├───reducer
        │   │       └───selector
        │   ├───risk-library
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       └───edit
        │   │   ├───models
        │   │   ├───services
        │   │   └───store
        │   │       ├───actions
        │   │       ├───effects
        │   │       ├───reducer
        │   │       └───selector
        │   ├───setting-rating
        │   │   ├───control
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       └───edit
        │   │   │   ├───models
        │   │   │   ├───services
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       ├───effects
        │   │   │       ├───reducer
        │   │   │       └───selector
        │   │   ├───ihrr-predicate
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       └───edit
        │   │   │   ├───models
        │   │   │   ├───services
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       ├───effects
        │   │   │       ├───reducer
        │   │   │       └───selector
        │   │   ├───impact
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       └───edit
        │   │   │   ├───models
        │   │   │   ├───services
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       ├───effects
        │   │   │       ├───reducer
        │   │   │       └───selector
        │   │   ├───likelihood
        │   │   │   ├───components
        │   │   │   │   └───main
        │   │   │   ├───models
        │   │   │   ├───services
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       ├───effects
        │   │   │       ├───reducer
        │   │   │       └───selector
        │   │   └───ratting-composite
        │   │       ├───components
        │   │       │   ├───main
        │   │       │   └───modals
        │   │       │       ├───add
        │   │       │       └───edit
        │   │       ├───models
        │   │       ├───services
        │   │       └───store
        │   │           ├───actions
        │   │           ├───effects
        │   │           ├───reducer
        │   │           └───selector
        │   ├───setting-top-risk
        │   │   ├───components
        │   │   │   └───main
        │   │   ├───models
        │   │   ├───services
        │   │   └───store
        │   │       ├───actions
        │   │       ├───effects
        │   │       ├───reducer
        │   │       └───selector
        │   └───top-risk
        │       ├───components
        │       │   ├───modals
        │       │   │   ├───add-ketentuan
        │       │   │   └───edit-ketentuan
        │       │   └───top-risk-component
        │       ├───models
        │       └───services
        └───user-management
            ├───role
            │   ├───market-risk
            │   │   ├───components
            │   │   │   ├───main
            │   │   │   └───modals
            │   │   │       ├───add
            │   │   │       └───edit
            │   │   ├───models
            │   │   ├───services
            │   │   └───store
            │   │       ├───actions
            │   │       ├───effects
            │   │       ├───reducer
            │   │       └───selector
            │   ├───operational-risk
            │   │   ├───components
            │   │   │   ├───main
            │   │   │   └───modals
            │   │   │       ├───add
            │   │   │       └───edit
            │   │   ├───models
            │   │   ├───services
            │   │   └───store
            │   │       ├───actions
            │   │       ├───effects
            │   │       ├───reducer
            │   │       └───selector
            │   └───risk-integration
            │       ├───components
            │       │   ├───main
            │       │   └───modals
            │       │       ├───add
            │       │       └───edit
            │       ├───models
            │       ├───services
            │       └───store
            │           ├───actions
            │           ├───effects
            │           ├───reducer
            │           └───selector
            ├───unit-kerja
            │   ├───jaringan
            │   │   ├───area
            │   │   │   ├───components
            │   │   │   │   ├───main
            │   │   │   │   └───modals
            │   │   │   │       ├───add
            │   │   │   │       └───edit
            │   │   │   ├───models
            │   │   │   ├───services
            │   │   │   └───store
            │   │   │       ├───actions
            │   │   │       ├───effects
            │   │   │       ├───reducer
            │   │   │       └───selector
            │   │   ├───cabang
            │   │   │   ├───components
            │   │   │   │   ├───main
            │   │   │   │   └───modals
            │   │   │   │       ├───add
            │   │   │   │       └───edit
            │   │   │   ├───models
            │   │   │   ├───services
            │   │   │   └───store
            │   │   │       ├───actions
            │   │   │       ├───effects
            │   │   │       ├───reducer
            │   │   │       └───selector
            │   │   ├───region
            │   │   │   ├───components
            │   │   │   │   ├───main
            │   │   │   │   └───modals
            │   │   │   │       ├───add
            │   │   │   │       └───edit
            │   │   │   ├───models
            │   │   │   ├───services
            │   │   │   └───store
            │   │   │       ├───actions
            │   │   │       ├───effects
            │   │   │       ├───reducer
            │   │   │       └───selector
            │   │   └───segmen
            │   │       ├───components
            │   │       │   ├───main
            │   │       │   └───modals
            │   │       │       ├───add
            │   │       │       └───edit
            │   │       ├───models
            │   │       ├───services
            │   │       └───store
            │   │           ├───actions
            │   │           ├───effects
            │   │           ├───reducer
            │   │           └───selector
            │   └───kantor-pusat
            │       ├───department
            │       │   ├───components
            │       │   │   ├───main
            │       │   │   └───modals
            │       │   │       ├───add
            │       │   │       └───edit
            │       │   ├───models
            │       │   ├───services
            │       │   └───store
            │       │       ├───actions
            │       │       ├───effects
            │       │       ├───reducer
            │       │       └───selector
            │       ├───directorate
            │       │   ├───components
            │       │   │   ├───main
            │       │   │   └───modals
            │       │   │       ├───add
            │       │   │       └───edit
            │       │   ├───models
            │       │   ├───services
            │       │   └───store
            │       │       ├───actions
            │       │       ├───effects
            │       │       ├───reducer
            │       │       └───selector
            │       └───group
            │           ├───components
            │           │   ├───main
            │           │   └───modals
            │           │       ├───add
            │           │       └───edit
            │           ├───models
            │           ├───services
            │           └───store
            │               ├───actions
            │               ├───effects
            │               ├───reducer
            │               └───selector
            ├───user
            │   ├───components
            │   │   ├───main
            │   │   └───modals
            │   │       ├───add
            │   │       └───edit
            │   ├───models
            │   ├───services
            │   └───store
            │       ├───actions
            │       ├───effects
            │       ├───reducer
            │       └───selector
            └───user-delegate
                ├───components
                │   ├───main
                │   └───modals
                │       ├───add
                │       └───edit
                ├───models
                ├───services
                └───store
                    ├───actions
                    ├───effects
                    ├───reducer
                    └───selector
```

## Detail folder

```sh
└───module
    └───rcsa
        ├───maintance
        │   ├───ketentuan
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   │       component.html    
        │   │   │   │       component.ts      
        │   │   │   │
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       │       component.html
        │   │   │       │       component.ts  
        │   │   │       │
        │   │   │       └───edit
        │   │   │               component.html
        │   │   │               component.ts  
        │   │   │
        │   │   ├───models
        │   │   │       interface.ts
        │   │   │
        │   │   ├───services
        │   │   │       service.ts
        │   │   │
        │   │   └───store
        │   │       ├───actions
        │   │       │       filename.action.ts
        │   │       │
        │   │       ├───effects
        │   │       │       filename.effect.ts
        │   │       │
        │   │       ├───reducer
        │   │       │       filename.reducer.ts
        │   │       │
        │   │       └───selector
        │   │               filename.selector.ts
        │   │
        │   ├───key-control
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   │       component.html
        │   │   │   │       component.ts
        │   │   │   │
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       │       component.html
        │   │   │       │       component.ts
        │   │   │       │
        │   │   │       └───edit
        │   │   │               component.html
        │   │   │               component.ts
        │   │   │
        │   │   ├───models
        │   │   │       interface.ts
        │   │   │
        │   │   ├───services
        │   │   │       service.ts
        │   │   │
        │   │   └───store
        │   │       ├───actions
        │   │       │       filename.action.ts
        │   │       │
        │   │       ├───effects
        │   │       │       filename.effect.ts
        │   │       │
        │   │       ├───reducer
        │   │       │       filename.reducer.ts
        │   │       │
        │   │       └───selector
        │   │               filename.selector.ts
        │   │
        │   ├───key-process
        │   │   ├───jaringan
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   │       component.html
        │   │   │   │   │       component.ts
        │   │   │   │   │
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       │       component.html
        │   │   │   │       │       component.ts
        │   │   │   │       │
        │   │   │   │       └───edit
        │   │   │   │               component.html
        │   │   │   │               component.ts
        │   │   │   │
        │   │   │   ├───models
        │   │   │   │       interface.ts
        │   │   │   │
        │   │   │   ├───services
        │   │   │   │       service.ts
        │   │   │   │
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       │       filename.action.ts
        │   │   │       │
        │   │   │       ├───effects
        │   │   │       │       filename.effect.ts
        │   │   │       │
        │   │   │       ├───reducer
        │   │   │       │       filename.reducer.ts
        │   │   │       │
        │   │   │       └───selector
        │   │   │               filename.selector.ts
        │   │   │
        │   │   └───kantor-pusat
        │   │       ├───components
        │   │       │   ├───main
        │   │       │   │       component.html
        │   │       │   │       component.ts
        │   │       │   │
        │   │       │   └───modals
        │   │       │       ├───add
        │   │       │       │       component.html
        │   │       │       │       component.ts
        │   │       │       │
        │   │       │       └───edit
        │   │       │               component.html
        │   │       │               component.ts
        │   │       │
        │   │       ├───models
        │   │       │       interface.ts
        │   │       │
        │   │       ├───services
        │   │       │       service.ts
        │   │       │
        │   │       └───store
        │   │           ├───actions
        │   │           │       filename.action.ts
        │   │           │
        │   │           ├───effects
        │   │           │       filename.effect.ts
        │   │           │
        │   │           ├───reducer
        │   │           │       filename.reducer.ts
        │   │           │
        │   │           └───selector
        │   │                   filename.selector.ts
        │   │
        │   ├───key-risk
        │   │   ├───jaringan
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   │       component.html
        │   │   │   │   │       component.ts
        │   │   │   │   │
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       │       component.html
        │   │   │   │       │       component.ts
        │   │   │   │       │
        │   │   │   │       └───edit
        │   │   │   │               component.html
        │   │   │   │               component.ts
        │   │   │   │
        │   │   │   ├───models
        │   │   │   │       interface.ts
        │   │   │   │
        │   │   │   ├───services
        │   │   │   │       service.ts
        │   │   │   │
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       │       filename.action.ts
        │   │   │       │
        │   │   │       ├───effects
        │   │   │       │       filename.effect.ts
        │   │   │       │
        │   │   │       ├───reducer
        │   │   │       │       filename.reducer.ts
        │   │   │       │
        │   │   │       └───selector
        │   │   │               filename.selector.ts
        │   │   │
        │   │   └───kantor-pusat
        │   │       ├───components
        │   │       │   ├───main
        │   │       │   │       component.html
        │   │       │   │       component.ts
        │   │       │   │
        │   │       │   └───modals
        │   │       │       ├───add
        │   │       │       │       component.html
        │   │       │       │       component.ts
        │   │       │       │
        │   │       │       └───edit
        │   │       │               component.html
        │   │       │               component.ts
        │   │       │
        │   │       ├───models
        │   │       │       interface.ts
        │   │       │
        │   │       ├───services
        │   │       │       service.ts
        │   │       │
        │   │       └───store
        │   │           ├───actions
        │   │           │       filename.action.ts
        │   │           │
        │   │           ├───effects
        │   │           │       filename.effect.ts
        │   │           │
        │   │           ├───reducer
        │   │           │       filename.reducer.ts
        │   │           │
        │   │           └───selector
        │   │                   filename.selector.ts
        │   │
        │   ├───let
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   │       component.html
        │   │   │   │       component.ts
        │   │   │   │
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       │       component.html
        │   │   │       │       component.ts
        │   │   │       │
        │   │   │       └───edit
        │   │   │               component.html
        │   │   │               component.ts
        │   │   │
        │   │   ├───models
        │   │   │       interface.ts
        │   │   │
        │   │   ├───services
        │   │   │       service.ts
        │   │   │
        │   │   └───store
        │   │       ├───actions
        │   │       │       filename.action.ts
        │   │       │
        │   │       ├───effects
        │   │       │       filename.effect.ts
        │   │       │
        │   │       ├───reducer
        │   │       │       filename.reducer.ts
        │   │       │
        │   │       └───selector
        │   │               filename.selector.ts
        │   │
        │   ├───risk-library
        │   │   ├───components
        │   │   │   ├───main
        │   │   │   │       component.html
        │   │   │   │       component.ts
        │   │   │   │       
        │   │   │   └───modals
        │   │   │       ├───add
        │   │   │       │       component.html
        │   │   │       │       component.ts
        │   │   │       │
        │   │   │       └───edit
        │   │   │               component.html
        │   │   │               component.ts
        │   │   │
        │   │   ├───models
        │   │   │       interface.ts
        │   │   │
        │   │   ├───services
        │   │   │       service.ts
        │   │   │
        │   │   └───store
        │   │       ├───actions
        │   │       │       filename.action.ts
        │   │       │
        │   │       ├───effects
        │   │       │       filename.effect.ts
        │   │       │
        │   │       ├───reducer
        │   │       │       filename.reducer.ts
        │   │       │
        │   │       └───selector
        │   │               filename.selector.ts
        │   │
        │   ├───setting-rating
        │   │   ├───control
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   │       component.html
        │   │   │   │   │       component.ts
        │   │   │   │   │
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       │       component.html
        │   │   │   │       │       component.ts
        │   │   │   │       │
        │   │   │   │       └───edit
        │   │   │   │               component.html
        │   │   │   │               component.ts
        │   │   │   │
        │   │   │   ├───models
        │   │   │   │       interface.ts
        │   │   │   │
        │   │   │   ├───services
        │   │   │   │       service.ts
        │   │   │   │
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       │       filename.action.ts
        │   │   │       │
        │   │   │       ├───effects
        │   │   │       │       filename.effect.ts
        │   │   │       │
        │   │   │       ├───reducer
        │   │   │       │       filename.reducer.ts
        │   │   │       │
        │   │   │       └───selector
        │   │   │               filename.selector.ts
        │   │   │
        │   │   ├───ihrr-predicate
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   │       component.html
        │   │   │   │   │       component.ts
        │   │   │   │   │
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       │       component.html
        │   │   │   │       │       component.ts
        │   │   │   │       │
        │   │   │   │       └───edit
        │   │   │   │               component.html
        │   │   │   │               component.ts
        │   │   │   │
        │   │   │   ├───models
        │   │   │   │       interface.ts
        │   │   │   │
        │   │   │   ├───services
        │   │   │   │       service.ts
        │   │   │   │
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       │       filename.action.ts
        │   │   │       │
        │   │   │       ├───effects
        │   │   │       │       filename.effect.ts
        │   │   │       │
        │   │   │       ├───reducer
        │   │   │       │       filename.reducer.ts
        │   │   │       │
        │   │   │       └───selector
        │   │   │               filename.selector.ts
        │   │   │
        │   │   ├───impact
        │   │   │   ├───components
        │   │   │   │   ├───main
        │   │   │   │   │       component.html
        │   │   │   │   │       component.ts
        │   │   │   │   │
        │   │   │   │   └───modals
        │   │   │   │       ├───add
        │   │   │   │       │       component.html
        │   │   │   │       │       component.ts
        │   │   │   │       │
        │   │   │   │       └───edit
        │   │   │   │               component.html
        │   │   │   │               component.ts
        │   │   │   │
        │   │   │   ├───models
        │   │   │   │       interface.ts
        │   │   │   │
        │   │   │   ├───services
        │   │   │   │       service.ts
        │   │   │   │
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       │       filename.action.ts
        │   │   │       │
        │   │   │       ├───effects
        │   │   │       │       filename.effect.ts
        │   │   │       │
        │   │   │       ├───reducer
        │   │   │       │       filename.reducer.ts
        │   │   │       │
        │   │   │       └───selector
        │   │   │               filename.selector.ts
        │   │   │
        │   │   ├───likelihood
        │   │   │   ├───components
        │   │   │   │   └───main
        │   │   │   │           component.html
        │   │   │   │           component.ts
        │   │   │   │
        │   │   │   ├───models
        │   │   │   │       interface.ts
        │   │   │   │
        │   │   │   ├───services
        │   │   │   │       service.ts
        │   │   │   │
        │   │   │   └───store
        │   │   │       ├───actions
        │   │   │       │       filename.action.ts
        │   │   │       │
        │   │   │       ├───effects
        │   │   │       │       filename.effect.ts
        │   │   │       │
        │   │   │       ├───reducer
        │   │   │       │       filename.reducer.ts
        │   │   │       │
        │   │   │       └───selector
        │   │   │               filename.selector.ts
        │   │   │
        │   │   └───ratting-composite
        │   │       ├───components
        │   │       │   ├───main
        │   │       │   │       component.html
        │   │       │   │       component.ts
        │   │       │   │
        │   │       │   └───modals
        │   │       │       ├───add
        │   │       │       │       component.html
        │   │       │       │       component.ts
        │   │       │       │
        │   │       │       └───edit
        │   │       │               component.html
        │   │       │               component.ts
        │   │       │
        │   │       ├───models
        │   │       │       interface.ts
        │   │       │
        │   │       ├───services
        │   │       │       service.ts
        │   │       │
        │   │       └───store
        │   │           ├───actions
        │   │           │       filename.action.ts
        │   │           │
        │   │           ├───effects
        │   │           │       filename.effect.ts
        │   │           │
        │   │           ├───reducer
        │   │           │       filename.reducer.ts
        │   │           │
        │   │           └───selector
        │   │                   filename.selector.ts
        │   │
        │   ├───setting-top-risk
        │   │   ├───components
        │   │   │   └───main
        │   │   │           component.html
        │   │   │           component.ts
        │   │   │
        │   │   ├───models
        │   │   │       interface.ts
        │   │   │
        │   │   ├───services
        │   │   │       service.ts
        │   │   │
        │   │   └───store
        │   │       ├───actions
        │   │       │       filename.action.ts
        │   │       │
        │   │       ├───effects
        │   │       │       filename.effect.ts
        │   │       │
        │   │       ├───reducer
        │   │       │       filename.reducer.ts
        │   │       │
        │   │       └───selector
        │   │               filename.selector.ts
        │   │
        │   └───top-risk
        │       ├───components
        │       │   ├───modals
        │       │   │   ├───add-ketentuan
        │       │   │   │       component.html
        │       │   │   │       component.ts
        │       │   │   │
        │       │   │   └───edit-ketentuan
        │       │   │           component.html
        │       │   │           component.ts
        │       │   │
        │       │   └───top-risk-component
        │       │           component.html
        │       │           component.ts
        │       │
        │       ├───models
        │       │       interface.ts
        │       │
        │       └───services
        │               service.ts
        │
        └───user-management
            ├───role
            │   ├───market-risk
            │   │   ├───components
            │   │   │   ├───main
            │   │   │   │       component.html
            │   │   │   │       component.ts
            │   │   │   │
            │   │   │   └───modals
            │   │   │       ├───add
            │   │   │       │       component.html
            │   │   │       │       component.ts
            │   │   │       │
            │   │   │       └───edit
            │   │   │               component.html
            │   │   │               component.ts
            │   │   │
            │   │   ├───models
            │   │   │       interface.ts
            │   │   │
            │   │   ├───services
            │   │   │       service.ts
            │   │   │
            │   │   └───store
            │   │       ├───actions
            │   │       │       filename.action.ts
            │   │       │
            │   │       ├───effects
            │   │       │       filename.effect.ts
            │   │       │
            │   │       ├───reducer
            │   │       │       filename.reducer.ts
            │   │       │
            │   │       └───selector
            │   │               filename.selector.ts
            │   │
            │   ├───operational-risk
            │   │   ├───components
            │   │   │   ├───main
            │   │   │   │       component.html
            │   │   │   │       component.ts
            │   │   │   │
            │   │   │   └───modals
            │   │   │       ├───add
            │   │   │       │       component.html
            │   │   │       │       component.ts
            │   │   │       │
            │   │   │       └───edit
            │   │   │               component.html
            │   │   │               component.ts
            │   │   │
            │   │   ├───models
            │   │   │       interface.ts
            │   │   │
            │   │   ├───services
            │   │   │       service.ts
            │   │   │
            │   │   └───store
            │   │       ├───actions
            │   │       │       filename.action.ts
            │   │       │
            │   │       ├───effects
            │   │       │       filename.effect.ts
            │   │       │
            │   │       ├───reducer
            │   │       │       filename.reducer.ts
            │   │       │
            │   │       └───selector
            │   │               filename.selector.ts
            │   │
            │   └───risk-integration
            │       ├───components
            │       │   ├───main
            │       │   │       component.html
            │       │   │       component.ts
            │       │   │
            │       │   └───modals
            │       │       ├───add
            │       │       │       component.html
            │       │       │       component.ts
            │       │       │
            │       │       └───edit
            │       │               component.html
            │       │               component.ts
            │       │
            │       ├───models
            │       │       interface.ts
            │       │
            │       ├───services
            │       │       service.ts
            │       │
            │       └───store
            │           ├───actions
            │           │       filename.action.ts
            │           │
            │           ├───effects
            │           │       filename.effect.ts
            │           │
            │           ├───reducer
            │           │       filename.reducer.ts
            │           │
            │           └───selector
            │                   filename.selector.ts
            │
            ├───unit-kerja
            │   ├───jaringan
            │   │   ├───area
            │   │   │   ├───components
            │   │   │   │   ├───main
            │   │   │   │   │       component.html
            │   │   │   │   │       component.ts
            │   │   │   │   │
            │   │   │   │   └───modals
            │   │   │   │       ├───add
            │   │   │   │       │       component.html
            │   │   │   │       │       component.ts
            │   │   │   │       │
            │   │   │   │       └───edit
            │   │   │   │               component.html
            │   │   │   │               component.ts
            │   │   │   │
            │   │   │   ├───models
            │   │   │   │       interface.ts
            │   │   │   │
            │   │   │   ├───services
            │   │   │   │       service.ts
            │   │   │   │
            │   │   │   └───store
            │   │   │       ├───actions
            │   │   │       │       filename.action.ts
            │   │   │       │
            │   │   │       ├───effects
            │   │   │       │       filename.effect.ts
            │   │   │       │
            │   │   │       ├───reducer
            │   │   │       │       filename.reducer.ts
            │   │   │       │
            │   │   │       └───selector
            │   │   │               filename.selector.ts
            │   │   │
            │   │   ├───cabang
            │   │   │   ├───components
            │   │   │   │   ├───main
            │   │   │   │   │       component.html
            │   │   │   │   │       component.ts
            │   │   │   │   │
            │   │   │   │   └───modals
            │   │   │   │       ├───add
            │   │   │   │       │       component.html
            │   │   │   │       │       component.ts
            │   │   │   │       │
            │   │   │   │       └───edit
            │   │   │   │               component.html
            │   │   │   │               component.ts
            │   │   │   │
            │   │   │   ├───models
            │   │   │   │       interface.ts
            │   │   │   │
            │   │   │   ├───services
            │   │   │   │       service.ts
            │   │   │   │
            │   │   │   └───store
            │   │   │       ├───actions
            │   │   │       │       filename.action.ts
            │   │   │       │
            │   │   │       ├───effects
            │   │   │       │       filename.effect.ts
            │   │   │       │
            │   │   │       ├───reducer
            │   │   │       │       filename.reducer.ts
            │   │   │       │
            │   │   │       └───selector
            │   │   │               filename.selector.ts
            │   │   │
            │   │   ├───region
            │   │   │   ├───components
            │   │   │   │   ├───main
            │   │   │   │   │       component.html
            │   │   │   │   │       component.ts
            │   │   │   │   │
            │   │   │   │   └───modals
            │   │   │   │       ├───add
            │   │   │   │       │       component.html
            │   │   │   │       │       component.ts
            │   │   │   │       │
            │   │   │   │       └───edit
            │   │   │   │               component.html
            │   │   │   │               component.ts
            │   │   │   │
            │   │   │   ├───models
            │   │   │   │       interface.ts
            │   │   │   │
            │   │   │   ├───services
            │   │   │   │       service.ts
            │   │   │   │
            │   │   │   └───store
            │   │   │       ├───actions
            │   │   │       │       filename.action.ts
            │   │   │       │
            │   │   │       ├───effects
            │   │   │       │       filename.effect.ts
            │   │   │       │
            │   │   │       ├───reducer
            │   │   │       │       filename.reducer.ts
            │   │   │       │
            │   │   │       └───selector
            │   │   │               filename.selector.ts
            │   │   │
            │   │   └───segmen
            │   │       ├───components
            │   │       │   ├───main
            │   │       │   │       component.html
            │   │       │   │       component.ts
            │   │       │   │
            │   │       │   └───modals
            │   │       │       ├───add
            │   │       │       │       component.html
            │   │       │       │       component.ts
            │   │       │       │
            │   │       │       └───edit
            │   │       │               component.html
            │   │       │               component.ts
            │   │       │
            │   │       ├───models
            │   │       │       interface.ts
            │   │       │
            │   │       ├───services
            │   │       │       service.ts
            │   │       │
            │   │       └───store
            │   │           ├───actions
            │   │           │       filename.action.ts
            │   │           │
            │   │           ├───effects
            │   │           │       filename.effect.ts
            │   │           │
            │   │           ├───reducer
            │   │           │       filename.reducer.ts
            │   │           │
            │   │           └───selector
            │   │                   filename.selector.ts
            │   │
            │   └───kantor-pusat
            │       ├───department
            │       │   ├───components
            │       │   │   ├───main
            │       │   │   │       component.html
            │       │   │   │       component.ts
            │       │   │   │
            │       │   │   └───modals
            │       │   │       ├───add
            │       │   │       │       component.html
            │       │   │       │       component.ts
            │       │   │       │
            │       │   │       └───edit
            │       │   │               component.html
            │       │   │               component.ts
            │       │   │
            │       │   ├───models
            │       │   │       interface.ts
            │       │   │
            │       │   ├───services
            │       │   │       service.ts
            │       │   │
            │       │   └───store
            │       │       ├───actions
            │       │       │       filename.action.ts
            │       │       │       
            │       │       ├───effects
            │       │       │       filename.effect.ts
            │       │       │
            │       │       ├───reducer
            │       │       │       filename.reducer.ts
            │       │       │
            │       │       └───selector
            │       │               filename.selector.ts
            │       │
            │       ├───directorate
            │       │   ├───components
            │       │   │   ├───main
            │       │   │   │       component.html
            │       │   │   │       component.ts
            │       │   │   │
            │       │   │   └───modals
            │       │   │       ├───add
            │       │   │       │       component.html
            │       │   │       │       component.ts
            │       │   │       │
            │       │   │       └───edit
            │       │   │               component.html
            │       │   │               component.ts
            │       │   │
            │       │   ├───models
            │       │   │       interface.ts
            │       │   │
            │       │   ├───services
            │       │   │       service.ts
            │       │   │
            │       │   └───store
            │       │       ├───actions
            │       │       │       filename.action.ts
            │       │       │
            │       │       ├───effects
            │       │       │       filename.effect.ts
            │       │       │
            │       │       ├───reducer
            │       │       │       filename.reducer.ts
            │       │       │
            │       │       └───selector
            │       │               filename.selector.ts
            │       │
            │       └───group
            │           ├───components
            │           │   ├───main
            │           │   │       component.html
            │           │   │       component.ts
            │           │   │
            │           │   └───modals
            │           │       ├───add
            │           │       │       component.html
            │           │       │       component.ts
            │           │       │
            │           │       └───edit
            │           │               component.html
            │           │               component.ts
            │           │
            │           ├───models
            │           │       interface.ts
            │           │
            │           ├───services
            │           │       service.ts
            │           │
            │           └───store
            │               ├───actions
            │               │       filename.action.ts
            │               │
            │               ├───effects
            │               │       filename.effect.ts
            │               │
            │               ├───reducer
            │               │       filename.reducer.ts
            │               │
            │               └───selector
            │                       filename.selector.ts
            │
            ├───user
            │   ├───components
            │   │   ├───main
            │   │   │       component.html
            │   │   │       component.ts
            │   │   │
            │   │   └───modals
            │   │       ├───add
            │   │       │       component.html
            │   │       │       component.ts
            │   │       │
            │   │       └───edit
            │   │               component.html
            │   │               component.ts
            │   │
            │   ├───models
            │   │       interface.ts
            │   │
            │   ├───services
            │   │       service.ts
            │   │
            │   └───store
            │       ├───actions
            │       │       filename.action.ts
            │       │
            │       ├───effects
            │       │       filename.effect.ts
            │       │
            │       ├───reducer
            │       │       filename.reducer.ts
            │       │
            │       └───selector
            │               filename.selector.ts
            │
            └───user-delegate
                ├───components
                │   ├───main
                │   │       component.html
                │   │       component.ts
                │   │
                │   └───modals
                │       ├───add
                │       │       component.html
                │       │       component.ts
                │       │
                │       └───edit
                │               component.html
                │               component.ts
                │
                ├───models
                │       interface.ts
                │
                ├───services
                │       service.ts
                │
                └───store
                    ├───actions
                    │       filename.action.ts
                    │
                    ├───effects
                    │       filename.effect.ts
                    │
                    ├───reducer
                    │       filename.reducer.ts
                    │
                    └───selector
                            filename.selector.ts
```
